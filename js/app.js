(function ($) {
	'use strict';

	// Avoid `console` errors in browsers that lack a console.
	var method,
		noop = function () {},
		methods = [
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed',
			'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeStamp',
			'trace', 'warn'
		],
		length = methods.length,
		console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];
		if (!console[method]) { // Only stub undefined methods.
			console[method] = noop;
		}
	}

	/**
	 * On ready
	 */
	$(function () {
		var questionId = 0,
			APP = {
				questions: {
					0: {
						text: 'Кто проживает на дне океана?',
						variants: {
							1: 'Спанч-Боб Сквэапэнтс',
							2: 'Желтая губка',
							3: 'Адольф',
							4: 'Бабушка удава',
							5: 'Малыш без изъяна'
						},
						answers:  [1, 2, 5]
					},
					1: {
						text: 'Кто вышивает всегда без изъяна?',
						variants: {
							1: 'Малыш без изъяна',
							2: 'Желтая губка',
							3: 'Спанч-Боб Сквэапэнтс',
							4: 'Бабушка удава',
							5: 'Бабушка Адольфа'
						},
						answers:  [1, 4, 5]
					}
				},
				userAnswers: {}
			};

		$('.js-draggable').draggable();
		$('.js-droppable').droppable({
			accept: '.js-draggable',
			tolerance: 'intersect',
			drop: function(evt, ui) {
				var answerValue = ui.draggable.data('value'),
					question = APP.questions[questionId];

				for (var idx = 0; idx < question.answers.length; idx++) {
					if (question.answers[idx] == answerValue) {
						// answer correct
						if (typeof APP.userAnswers[questionId] === 'undefined') {
							APP.userAnswers[questionId] = [];
						}

						APP.userAnswers[questionId].push(answerValue);
						var answerCnt = APP.userAnswers[questionId].length;
						ui.draggable.addClass('selected');
						$('.js-question-answers').attr('data-state', answerCnt);
						initText(answerCnt, question.variants[answerValue]);
						return;
					}
				}

				// wrong answer
				$(this).removeClass('over');
				ui.draggable.addClass('fail');
				setTimeout(function () {
					ui.draggable.removeClass('fail');
				}, 1500);
			},
			deactivate: function(evt, ui) {
				$(this).removeClass('over');
				ui.draggable.css({
					left: '',
					top: ''
				});
			},
			over: function (evt, ui) {
				$(this).addClass('over');
			},
			out: function (evt, ui) {
				$(this).removeClass('over');
			}
		});

		/**
		 * Init round text
		 * @param idx integer (1 | 2 | 3)
		 * @param text string
		 */
		function initText(idx, text) {
			if (idx == 1) {
				$('.js-answer-1 b').html(text).elipText({ radius: 135 });
			}
			if (idx == 2) {
				$('.js-answer-2 b').html(text).elipText({ radius: 170 });
			}
			if (idx == 2) {
				$('.js-answer-3 b').html(text).elipText({ radius: 210 });
			}
		}

		//+ Jonas Raoni Soares Silva
		//@ http://jsfromhell.com/array/shuffle [v1.0]
		function shuffle(o) { //v1.0
			for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		}

		/**
		 * Init scene for question
		 */
		function initQuestion() {
			var $answer = $('.js-draggable').first(),
				behaviorArrValues = [1, 2, 3, 4, 5],
				behaviorArr = shuffle(behaviorArrValues.slice());

			$.each(APP.questions[questionId].variants, function (key, item) {
				// add random behavior class
				$.each(behaviorArrValues, function (key, value) {
					$answer.removeClass('answer-behavior-' + value);
				});
				$answer.addClass('answer-behavior-' + behaviorArr.pop());


				// show answer & add text
				$answer.show().data('value', key).removeClass('selected')
					.find('span').html(item);

				$answer = $answer.next();
			});

			$answer.prev().nextAll().hide();

			$('.js-question-answers').attr('data-state', '0');
		}

		// next question
		$('.js-next').on('click', function () {
			// add points @todo

			$.ajax({
				dataType: 'json',
				url: 'ajax/1.json',
				data: APP.userAnswers[questionId],
				success: function () {
					questionId++;
					initQuestion();
					// add points @todo
				},
				error: function () {
					// add points or show error @todo
				}
			});
		});

		/*
		$('.js-answer-1 b').elipText({ radius: 135 });
		$('.js-answer-2 b').elipText({ radius: 170 });
		$('.js-answer-3 b').elipText({ radius: 210 });
		*/

		initQuestion();
	});
}(jQuery));
