module.exports = function (grunt) {

  /**
   * Окружение
   */
  if (grunt.option('env') !== 'prod') {
    grunt.option('env', 'dev');
  }

  grunt.log.ok('Environment: ' + grunt.option('env'));

  /**
   * Конфигурация проекта
   */
  grunt.initConfig(
    {
      pkg: grunt.file.readJSON('package.json'),
      sass: {
        compile: {
          options: {
            outputStyle: (grunt.option('env') === 'dev' ? 'nested' : 'compressed'),
            sourceMap: (grunt.option('env') === 'dev')
          },
          files:   {
            './css/main.css': './css/main.scss'
          }
        }
      },
      gaze: {
        scss: {
          files: [
            './css/**/*.scss'
          ],
          tasks: ['sass', 'autoprefixer']
        }
      },
      autoprefixer: {
        options: {},
        prfx: {
          src: './css/main.css'
        }
      }
    }
  );

  /**
   * Плагины
   */
  // Отслеживает изменения файлов. (grunt-contrib-watch не работал на Win7)
  grunt.loadNpmTasks('grunt-gaze');

  // Сборщик для sass
  grunt.loadNpmTasks('grunt-sass');

  // autoprefixer
  grunt.loadNpmTasks('grunt-autoprefixer');

  /**
   * Задачи
   */
  grunt.registerTask(
    'build', 'Build project', function () {
      grunt.task.run(['sass', 'autoprefixer']);
    }
  );

  grunt.registerTask(
    'default', 'Build sprite and cscc. Watch changes in images and *.styl files', function () {
      // Этот пакет задач не отваливается при предупреждениях (критично для gaze)
      grunt.option('force', true);
      grunt.task.run(['build', 'gaze']);
    }
  );
};
